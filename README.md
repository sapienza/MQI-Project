---------------------USAGE OF NEURAL NETWORKS IN PAGE BLOCK CLASSIFICATION------------------

Our purpose is to train a Neural Network with a Dataset (https://archive.ics.uci.edu/ml/datasets/Page+Blocks+Classification) and using it to identify the right type of page layout.
This is a Supervised Classification problem.


Prerequisites:
MATLAB R2015a

Authors:
Lo Presti Alessandro (lopresti.1648663@studenti.uniroma1.it)
Longo Valerio (longo.1655653@studenti.uniroma1.it)

Installing:
Be careful that page-blocks.txt and pageBlocks.m are in the same directory.


Set the Matlab current folder in the same directory of page-blocks.txt.


Other Notes:

This project is focused about the Neural Network analysis and its behaviour when we change the parameters, with an explanation of the results.


We will use Matlab How Programming Language, specifically: "Matlab Neural Networks Toolbox"


